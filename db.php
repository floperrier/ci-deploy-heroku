<?php

//require_once './config.php';

class DB
{
    protected static $instance;

    protected function __construct()
    {
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {


            $parts = (parse_url(getenv('HEROKU_POSTGRESQL_PINK_URL')));

            // Extrait variables scheme, host, port, user, pass, path
            var_dump($parts);
            extract($parts);

            $path = ltrim($path, "/");

            try {
                self::$instance = new PDO("pgsql:host={$host};port={$port};dbname={$path}", $user, $pass);
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                self::$instance->query('SET NAMES utf8');
                self::$instance->query('SET CHARACTER SET utf8');
            } catch (PDOException $error) {
                echo $error->getMessage();
            }
        }

        return self::$instance;
    }
}
